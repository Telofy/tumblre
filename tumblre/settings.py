# -*- encoding: utf-8 -*-
from __future__ import (
    unicode_literals, absolute_import, print_function, division)

CONSUMER_KEY = None
CONSUMER_SECRET = None
OAUTH_TOKEN = None
OAUTH_SECRET = None

try:
    from .settings_override import *
except ImportError:
    pass
