# -*- encoding: utf-8 -*-
from __future__ import (
    unicode_literals, absolute_import, print_function, division)
import logging

logging.basicConfig(
    format='%(asctime)s: %(levelname)s:'
           ' %(funcName)s (%(thread)d):'
           ' %(message)s')
logger = logging.getLogger(__name__.rsplit('.', 1)[0])
logger.setLevel(logging.INFO)
