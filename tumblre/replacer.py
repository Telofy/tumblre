# -*- encoding: utf-8 -*-
from __future__ import (
    unicode_literals, absolute_import, print_function, division)
from collections import defaultdict
from copy import copy
from tumblpy import Tumblpy
from .logger import logger
from . import settings

def multisub(repltuples, value):
    for regex, repl in repltuples:
        value = regex.sub(repl, value)
    return value


class Replacer(object):

    def __init__(self, *args, **kwargs):
        self._tumblr = Tumblpy(*args, **kwargs)
        self._blog = None
        self._post_ids = None
        self._repls = None

    def blog(self, name):
        _self = copy(self)
        _self._blog = name
        return _self

    def posts(self, *args):
        _self = copy(self)
        _self._post_ids = args
        return _self

    def replace(self, **kwargs):
        _self = copy(self)
        _self._repls = _self._repls or defaultdict(list)
        for key, repltuple in kwargs.items():
            _self._repls[key].append(repltuple)
        return _self

    def _generate(self):
        for post_id in self._post_ids:
            response = self._tumblr.get(
                'posts', self._blog, params={'id': post_id})
            post = response['posts'][0]
            extraneous = set(self._repls.keys()) - set(post.keys())
            if extraneous:
                logger.warn('Keys %s incompatible with %s',
                            list(extraneous), post_id)
            update = {key: multisub(repltuples, post[key])
                      for key, repltuples in self._repls.items()
                      if post.has_key(key)}
            if update:
                update.update({'id': post_id})
                yield update

    def simulate(self):
        for update in self._generate():
            logger.info('Hypothetical update: %s', update)

    def execute(self):
        for update in self._generate():
            self._tumblr.post('post/edit', self._blog, params=update)
            logger.info('Post updated: %s', update)


def run():
    import re
    import IPython
    replacer = Replacer(settings.CONSUMER_KEY, settings.CONSUMER_SECRET,
                        settings.OAUTH_TOKEN, settings.OAUTH_SECRET)
    IPython.embed()
