=======
Tumblre
=======

A simple tool or library for replacing text in Tumblr posts using regular expressions.

You can define your OAuth credentials in ``tumblre/settings_override.py`` (which does not exist).

Example usage (``re`` is imported by default)::

    $ bin/replacer
    ...
    >>> replacer.blog('wandernews.claviger.net') \
            .posts(61130985800, 61114828675, 61062615110) \
            .replace(body=(re.compile('Twilight'), 'T. Sparkle')) \
            .replace(caption=(re.compile('<h2>'), '<h2 itemprop="name">')) \
            .simulate()
    ...
    >>> replacer.blog('wandernews.claviger.net') \
            .posts(61130985800, 61114828675, 61062615110) \
            .replace(body=(re.compile('Twilight'), 'T. Sparkle')) \
            .replace(caption=(re.compile('<h2>'), '<h2 itemprop="name">')) \
            .execute()

Keys that do not exist in posts are skipped. If all keys were skipped, the post is not updated, but the processing proceeds with the next post.
