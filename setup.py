#!/usr/bin/env python
from setuptools import setup

setup(
    name='tumblre',
    version='0.0.1',
    author='Telofy',
    author_email='tumblre@yu-shin.de',
    include_package_data=True,
    extras_require=dict(
        test=['coverage>=3.4'],
    ),
    install_requires=[
        'setuptools',
        'python-tumblpy>=1.0.3',
        'IPython>=1.1.0',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'replacer = tumblre.replacer:run'
        ]
    }
)
